//
// Created by serhii on 24.04.20.
//

#ifndef CHESS_FIGURES_HPP
#define CHESS_FIGURES_HPP

#include <functional>
#include <memory>

namespace chess{

enum class Colour{
    White,
    Black
};

enum class Type{
    Pawn,	//пешка
    King,	//король
    Queen,	//ферзь (дама)
    Rook,	//ладья (тура)
    Knight,	//конь
    Bishop  //слон (офицер)
};

//function that return true if there is no figures in this cell
using is_empty_cell = std::function<bool(int, int)>;

using is_enemy_in_cell = std::function<bool(int, int, Colour)>;

class Figure{
protected:
    const Colour colour;
    const Type type;
    int pos_x;
    int pos_y;
    int move_cnt = 0;
public:
    Figure(Colour c, Type t,  int x, int y, int move_cnt=0);
    Colour getColour() const noexcept;
    Type getType() const noexcept;
    int& getMoveCnt() noexcept;
    int getMoveCnt() const noexcept;
    bool isAllyOnNewCell(int new_x, int new_y, const is_empty_cell& isEmptyCell, const is_enemy_in_cell& isEnemyInCell) const noexcept ;
    void setNewCell(int x, int y) noexcept ;//Warning! this method don't check ability to set a new cell
    virtual bool isMoveValid(int new_x, int new_y, const is_empty_cell& isEmptyCell, const is_enemy_in_cell& isEnemyInCell)=0;
    virtual ~Figure(){};
};

class Pawn: public Figure{
public:
    Pawn(Colour c, int x, int y, int move_cnt=0);
    bool isMoveValid(int new_x, int new_y, const is_empty_cell& isEmptyCell, const is_enemy_in_cell& isEnemyInCell) override final;
    ~Pawn(){};
};

class Rook: public Figure{
public:
    Rook(Colour c, int x, int y, int move_cnt=0);
    bool isMoveValid(int new_x, int new_y, const is_empty_cell& isEmptyCell, const is_enemy_in_cell& isEnemyInCell) override final;
    ~Rook(){};
};

class Knight: public Figure{
public:
    Knight(Colour c, int x, int y, int move_cnt=0);
    bool isMoveValid(int new_x, int new_y, const is_empty_cell& isEmptyCell, const is_enemy_in_cell& isEnemyInCell) override final;
    ~Knight(){};
};

class Bishop: public Figure{
public:
    Bishop(Colour c, int x, int y, int move_cnt=0);
    bool isMoveValid(int new_x, int new_y, const is_empty_cell& isEmptyCell, const is_enemy_in_cell& isEnemyInCell) override final;
    ~Bishop(){};
};

class Queen: public Figure{
public:
    Queen(Colour c, int x, int y, int move_cnt=0);
    bool isMoveValid(int new_x, int new_y, const is_empty_cell& isEmptyCell, const is_enemy_in_cell& isEnemyInCell) override final;
    ~Queen(){};
};

class King: public Figure{
public:
    King(Colour c, int x, int y, int move_cnt=0);
    bool isMoveValid(int new_x, int new_y, const is_empty_cell& isEmptyCell, const is_enemy_in_cell& isEnemyInCell) override final;
    ~King(){};
};

}

#endif //CHESS_FIGURES_HPP
