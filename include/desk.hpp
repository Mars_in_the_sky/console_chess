//
// Created by serhii on 24.04.20.
//

#ifndef CHESS_DESK_HPP
#define CHESS_DESK_HPP

#include "figures.hpp"

#include <list>

namespace chess {

struct Cell{
    std::unique_ptr<Figure> fig = nullptr;
};

enum class CastlingType{
    Long,
    Short
};

enum class GameExodus{
    Win,
    Stalemate,
    Change,
    None
};

using cells_array = std::array<std::array<Cell, 9>, 9>; //we make 9x9 but we will ignore 0; (for ease of development)
//using validate_functions = std::vector<std::pair<Figure, std::function<bool(int, int, int, int, cells_array &)>>>;

class Desk {
#ifdef TESTING
public:
#endif
    //std::vector<chess::Figure> figures; //I would prefer to use here boost::container::static_vector but i can't :(
    cells_array cells;
    //validate_functions figure_move_map;
    bool isEmptyCell(int x, int y);
    bool isEnemyInCell(int x, int y, Colour self_colour);
    bool isCheck(Colour king_colour);
    bool isWin(Colour win_colour);
    bool isStalemate(Colour win_colour);
    bool isAnyStepToSaveKing(int fig_x, int fig_y, Colour colour);
    bool isAnyStep(int fig_x, int fig_y, Colour colour);
    std::unique_ptr<Figure> tryToMakeMove(int old_x, int old_y, int new_x, int new_y, Colour player_colour);
    bool isPawnChange(Colour player);
public:
    GameExodus makeMove(int old_x, int old_y, int new_x, int new_y, Colour player_colour);
    void makeCastling(CastlingType cast_type, Colour player);
    void makePawnChange(Colour player, Type type);
    bool canFieldBeFightByEnemy(int field_x, int field_y, Colour enemy_colour);
    void visualization();
    Desk();
};

inline Colour getEnemyColour(Colour colour){
    return (colour==Colour::White)?Colour::Black:Colour::White;
}
}//namespace chess
#endif //CHESS_DESK_HPP
