//
// Created by serhii on 28.04.20.
//

#ifndef CHESS_GAMES_STORAGE_HPP
#define CHESS_GAMES_STORAGE_HPP

#include <filesystem>
#include <vector>
#include <fstream>

namespace chess{

class GamesStorage{
    std::filesystem::path resources;
    std::ofstream output;
    long game_id=0;
public:
    GamesStorage(std::string& path);
    std::vector<std::string> getGame(long id);
    void addActionToCurrGame(std::string& act);
    long startGame(long id=0);
    long finishGame();
    long getGameId() const noexcept;

};
}

#endif //CHESS_GAMES_STORAGE_HPP
