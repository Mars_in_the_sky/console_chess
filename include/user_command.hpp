//
// Created by serhii on 25.04.20.
//

#ifndef CHESS_USER_COMMAND_HPP
#define CHESS_USER_COMMAND_HPP

#include <optional>
#include "desk.hpp"
#include "games_storage.hpp"
#include "figures.hpp"

namespace chess {

struct Move{
    int old_x;
    int old_y;
    int new_x;
    int new_y;
};

enum class CommandType{
    Move,
    Castling,
    Save,
    Load,
    NewGame,
    Help,
    Undefined
};

class UserCommand {
    Desk desk;
    Colour current_player_colour = Colour::White;
    bool game_running = false;
    const static std::unordered_map<std::string, CommandType> commands;
    static std::pair<std::string, CommandType> define_command_type(std::string& input_str);
    static std::optional<Move> parse_move(std::string &input_str);
    static std::optional<CastlingType> parse_castling(std::string& input_str);
    bool make_move(std::string &input_str);
    bool make_castling(std::string &input_str);
    void make_save();
    bool make_load(std::string &input_str);
    void init_new_game();
    static void help();
    static void undefined();
    bool makeCommand(std::string& input_str);
    bool makeStartingCommand(std::string& input_str);
    void makeChange();
public:
    void visualization();

    [[noreturn]] void run();
    GamesStorage storage;
    UserCommand(std::string working_directory);
};


}//namespace

#endif //CHESS_USER_COMMAND_HPP
