//
// Created by serhii on 29.04.20.
//
#define TESTING
#include "desk.hpp"
#include <iostream>
#include <cassert>

using namespace std;
using namespace chess;

#define SUCCESS cout<<__FUNCTION__<<" - success!"<<std::endl

char resolver(int x){
    return 'A'+x-1;
}

void eraseDesk(Desk& d){
    std::array<std::array<Cell, 9>, 9> new_arr;
    d.cells = std::move(new_arr);
}

void test_canFieldBeFightByEnemy(){
    Desk d;
    eraseDesk(d);

    d.cells[1][8].fig = std::make_unique<Rook>(Colour::Black, 1, 8);
    d.cells[2][2].fig = std::make_unique<Knight>(Colour::Black, 2, 2);
    d.cells[5][8].fig = std::make_unique<King>(Colour::Black, 5, 8);
    d.cells[8][5].fig = std::make_unique<Queen>(Colour::White, 8, 5);

    vector<std::pair<int, int>> res_white;
    vector<std::pair<int, int>> res_black;
    for (int x=1;x<=8;x++){
        for (int y=1;y<=8;y++){
            if (d.canFieldBeFightByEnemy(x, y, chess::Colour::White)){
                res_white.emplace_back(x, y);
            }
        }
    }

    for (int x=1;x<=8;x++){
        for (int y=1;y<=8;y++){
            if (d.canFieldBeFightByEnemy(x, y, chess::Colour::Black)){
                res_black.emplace_back(x, y);
            }
        }
    }

    vector<std::pair<int, int>> needed_black = {{1,1},
                                                {1,2},
                                                {1,3},
                                                {1,4},
                                                {1,5},
                                                {1,6},
                                                {1,7},
                                                {2,8},
                                                {3,4},
                                                {3,8},
                                                {4,1},
                                                {4,3},
                                                {4,7},
                                                {4,8},
                                                {5,7},
                                                {6,7},
                                                {6,8}};

    vector<std::pair<int, int>> needed_white = {{1,5},
                                {2,5},
                                {3,5},
                                {4,1},
                                {4,5},
                                {5,2},
                                {5,5},
                                {5,8},
                                {6,3},
                                {6,5},
                                {6,7},
                                {7,4},
                                {7,5},
                                {7,6},
                                {8,1},
                                {8,2},
                                {8,3},
                                {8,4},
                                {8,6},
                                {8,7},
                                {8,8}};
    assert(res_white == needed_white);
    assert(res_black==needed_black);
    SUCCESS;
    //d.visualization();
}

void test_Pawn(){
    Desk d;
    eraseDesk(d);

    d.cells[2][2].fig = std::make_unique<Pawn>(Colour::White, 2, 2);
    d.cells[3][3].fig = std::make_unique<Pawn>(Colour::Black, 3,3);

    assert(d.cells[2][2].fig->isMoveValid(2, 3,
            std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
            std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(d.cells[2][2].fig->isMoveValid(2, 4,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(!d.cells[2][2].fig->isMoveValid(2, 5,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(d.cells[2][2].fig->isMoveValid(3, 3,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(!d.cells[2][2].fig->isMoveValid(4, 4,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(d.cells[3][3].fig->isMoveValid(3, 2,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    SUCCESS;
}

void test_Rook(){
    Desk d;
    eraseDesk(d);

    d.cells[2][2].fig = std::make_unique<Rook>(Colour::White, 2, 2);
    d.cells[3][3].fig = std::make_unique<Rook>(Colour::Black, 3,3);

    assert(d.cells[2][2].fig->isMoveValid(2, 8,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(d.cells[2][2].fig->isMoveValid(4, 2,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(!d.cells[2][2].fig->isMoveValid(8, 8,
                                           std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                           std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(d.cells[2][2].fig->isMoveValid(2, 1,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(!d.cells[3][3].fig->isMoveValid(4, 4,
                                           std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                           std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(d.cells[3][3].fig->isMoveValid(3, 2,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    SUCCESS;
}

void test_King(){
    Desk d;
    eraseDesk(d);

    d.cells[2][2].fig = std::make_unique<King>(Colour::White, 2, 2);

    assert(d.cells[2][2].fig->isMoveValid(2, 3,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(d.cells[2][2].fig->isMoveValid(2, 1,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(d.cells[2][2].fig->isMoveValid(3, 3,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(!d.cells[2][2].fig->isMoveValid(4, 4,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    SUCCESS;

}
void test_Queen(){
    Desk d;
    eraseDesk(d);

    d.cells[2][2].fig = std::make_unique<Queen>(Colour::White, 2, 2);

    assert(d.cells[2][2].fig->isMoveValid(8, 8,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(d.cells[2][2].fig->isMoveValid(1, 1,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(d.cells[2][2].fig->isMoveValid(2, 5,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(!d.cells[2][2].fig->isMoveValid(4, 8,
                                           std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                           std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    SUCCESS;
}

void test_Bishop(){
    Desk d;
    eraseDesk(d);

    d.cells[2][2].fig = std::make_unique<Bishop>(Colour::White, 2, 2);

    assert(d.cells[2][2].fig->isMoveValid(3, 3,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(d.cells[2][2].fig->isMoveValid(1, 1,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(d.cells[2][2].fig->isMoveValid(6, 6,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(!d.cells[2][2].fig->isMoveValid(4, 5,
                                           std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                           std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));

    eraseDesk(d);

    d.cells[4][4].fig = std::make_unique<Bishop>(Colour::White, 4, 4);

    assert(d.cells[4][4].fig->isMoveValid(8, 8,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(d.cells[4][4].fig->isMoveValid(1, 7,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(d.cells[4][4].fig->isMoveValid(7, 1,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(d.cells[4][4].fig->isMoveValid(1, 1,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));



    SUCCESS;
}

void test_Khight(){
    Desk d;
    eraseDesk(d);

    d.cells[2][2].fig = std::make_unique<Knight>(Colour::White, 2, 2);

    assert(d.cells[2][2].fig->isMoveValid(4, 1,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(d.cells[2][2].fig->isMoveValid(4, 3,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(d.cells[2][2].fig->isMoveValid(3, 4,
                                          std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                          std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    assert(!d.cells[2][2].fig->isMoveValid(4, 4,
                                           std::bind(&Desk::isEmptyCell, &d, std::placeholders::_1, std::placeholders::_2),
                                           std::bind(&Desk::isEnemyInCell, &d, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)));
    SUCCESS;
}

void test_Check(){
    Desk d;
    eraseDesk(d);

    d.cells[2][2].fig = std::make_unique<Bishop>(Colour::White, 2, 2);
    d.cells[3][3].fig = std::make_unique<King>(Colour::Black, 3,3);

    assert(d.isCheck(Colour::Black));

    eraseDesk(d);

    d.cells[2][2].fig = std::make_unique<King>(Colour::White, 2, 2);
    d.cells[4][3].fig = std::make_unique<Queen>(Colour::Black, 4,3);

    assert(!d.isCheck(Colour::White));

    SUCCESS;
}

void test_isAnyStepToSaveKing(){
    Desk d;
    eraseDesk(d);

    d.cells[2][2].fig = std::make_unique<Rook>(Colour::White, 2, 2);
    d.cells[1][2].fig = std::make_unique<Rook>(Colour::White, 1, 2);
    d.cells[1][8].fig = std::make_unique<King>(Colour::Black, 1,8);
    d.cells[8][5].fig = std::make_unique<Queen>(Colour::Black, 8, 5);



    assert(d.isAnyStepToSaveKing(8,5,Colour::Black));

    d.cells[7][5].fig = std::make_unique<Pawn>(Colour::Black, 7, 5);

    assert(!d.isAnyStepToSaveKing(8,5,Colour::Black));

    SUCCESS;
}

void test_isWin(){
    Desk d;
    eraseDesk(d);

    d.cells[2][2].fig = std::make_unique<Rook>(Colour::White, 2, 2);
    d.cells[1][2].fig = std::make_unique<Rook>(Colour::White, 1, 2);
    d.cells[1][8].fig = std::make_unique<King>(Colour::Black, 1,8);
    d.cells[8][5].fig = std::make_unique<Queen>(Colour::Black, 8, 5);

    assert(!d.isWin(chess::Colour::White));

    d.cells[7][5].fig = std::make_unique<Pawn>(Colour::Black, 7, 5);

    assert(d.isWin(chess::Colour::White));
    SUCCESS;
}

void test_isStalemate(){
    Desk d;
    eraseDesk(d);

    d.cells[2][2].fig = std::make_unique<Rook>(Colour::White, 2, 2);
    d.cells[1][2].fig = std::make_unique<Rook>(Colour::White, 1, 2);
    d.cells[1][8].fig = std::make_unique<King>(Colour::Black, 1,8);
    d.cells[1][7].fig = std::make_unique<Knight>(Colour::Black, 1, 7);

    assert(d.isStalemate(chess::Colour::White));

    d.cells[1][6].fig = std::make_unique<Pawn>(Colour::Black, 1, 6);

    assert(!d.isStalemate(chess::Colour::White));
    SUCCESS;
}

void test_castling(){
    Desk d;
    eraseDesk(d);

    d.cells[5][1].fig = std::make_unique<King>(Colour::White, 5, 1);
    d.cells[1][1].fig = std::make_unique<Rook>(Colour::White, 1, 1);

    d.makeCastling(CastlingType::Long, chess::Colour::White);

    assert(d.cells[3][1].fig->getType()==Type::King);
    assert(d.cells[4][1].fig->getType()==Type::Rook);

    eraseDesk(d);

    d.cells[5][1].fig = std::make_unique<King>(Colour::White, 5, 1);
    d.cells[1][1].fig = std::make_unique<Rook>(Colour::White, 1, 1);
    d.cells[8][1].fig = std::make_unique<Rook>(Colour::White, 8, 1);
    d.cells[6][3].fig = std::make_unique<Knight>(Colour::Black, 6, 3);

    bool res=false;
    try{
        d.makeCastling(CastlingType::Short, chess::Colour::White);
    }
    catch (...) {
        res= true;
    }
    assert (res);
    SUCCESS;
}

void test_makePawnChange(){
    Desk d;
    eraseDesk(d);

    d.cells[5][8].fig = std::make_unique<Pawn>(Colour::White, 5, 8);

    assert(d.isPawnChange(chess::Colour::White));

    d.makePawnChange(chess::Colour::White, Type::Rook);

    assert(d.cells[5][8].fig->getType()==Type::Rook);

    SUCCESS;
}


int main(){
    test_canFieldBeFightByEnemy();
    test_Pawn();
    test_Rook();
    test_King();
    test_Queen();
    test_Bishop();
    test_Khight();
    test_Check();
    test_isAnyStepToSaveKing();
    test_isWin();
    test_isStalemate();
    test_castling();
    test_makePawnChange();
    SUCCESS;
    return 0;
}

