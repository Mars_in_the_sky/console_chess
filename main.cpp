#include <iostream>
#include <string>
#include "include/user_command.hpp"
#include "include/desk.hpp"

#include <memory>
#include <zconf.h>
//#include <filesystem>

int main(int argc, char** argv) {
    if (argc==2){
        chess::UserCommand cmd( argv[1]);
        cmd.run();
    }
    else {
        std::cerr<<"Wrong parameters!"<<std::endl;
        std::cout<<"Please add working directory:\n"
            <<"\t example: '"<<argv[0]<<" chess_working_directory/'\n"<<std::endl;
    }
    return 0;
}
