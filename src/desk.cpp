//
// Created by serhii on 24.04.20.
//

#include "desk.hpp"
#include "figures.hpp"

#include <iostream>
#include <memory>
#include <sstream>
#include <cassert>

namespace chess {

inline char horizontal_to_char(int x){
    return 'A'+x-1;
}

inline std::string tag_resolver(chess::Figure* fig){
    std::string tag;
    switch (fig->getColour()) {
        case Colour::White:
            tag += "W";
            break;
        case Colour::Black:
            tag += "B";
            break;
        default:
            assert(false);
    }
    switch (fig->getType()) {
        case Type::Pawn:
            tag+="P";
            break;
        case Type::King:
            tag+="K";
            break;
        case Type::Queen:
            tag+="Q";
            break;
        case Type::Rook:
            tag+="R";
            break;
        case Type::Knight:
            tag+="N";
            break;
        case Type::Bishop:
            tag+="B";
            break;
        default:
            assert(false);
    }
    return tag;
}

void Desk::visualization() {
    std::stringstream ss;
    ss<<"  ";
    for (int x=1; x<=8; x++) {
        ss<<"    "<<horizontal_to_char(x)<<"    ";
    }
    ss<<"\n  ";
    for (int y = 8; y>=1; y--){
        for (int x=1; x<=8; x++) {
            ss<< "---------";
        }
        ss<<"|\n  ";
        for (int x=1; x<=8; x++) {
            ss<< "|        ";
        }
        ss<<"|\n"<<y<<" ";
        for (int x=1; x<=8; x++) {
            std::string label= (cells[x][y].fig)?(tag_resolver(cells[x][y].fig.get())):"  ";
            ss<< "|   "<<label<<"   ";
        }
        ss<<"|\n  ";
        for (int x=1; x<=8; x++) {
            ss<< "|        ";
        }
        ss<<"|\n  ";
    }
    ss<<"------------------------------------------------------------------------|\n  ";
    for (int x=1; x<=8; x++) {
        ss<<"    "<<horizontal_to_char(x)<<"    ";
    }
    ss<<"\n";
    std::cout<<ss.str();
}

Desk::Desk() {
    for (int i = 1; i<=8; i++) {
        cells[i][2].fig = std::make_unique<Pawn>(Colour::White, i, 2);
    }
    for (int i = 1; i<=8; i++) {
        cells[i][7].fig = std::make_unique<Pawn>(Colour::Black, i, 7);
    }

    cells[1][8].fig = std::make_unique<Rook>(Colour::Black, 1, 8);
    cells[8][8].fig = std::make_unique<Rook>(Colour::Black, 8, 8);
    cells[1][1].fig = std::make_unique<Rook>(Colour::White, 1, 1);
    cells[8][1].fig = std::make_unique<Rook>(Colour::White, 8, 1);

    cells[2][8].fig = std::make_unique<Knight>(Colour::Black, 2, 8);
    cells[7][8].fig = std::make_unique<Knight>(Colour::Black, 7, 8);
    cells[2][1].fig = std::make_unique<Knight>(Colour::White, 2, 1);
    cells[7][1].fig = std::make_unique<Knight>(Colour::White, 7, 1);

    cells[3][8].fig = std::make_unique<Bishop>(Colour::Black, 3, 8);
    cells[6][8].fig = std::make_unique<Bishop>(Colour::Black, 6, 8);
    cells[3][1].fig = std::make_unique<Bishop>(Colour::White, 3, 1);
    cells[6][1].fig = std::make_unique<Bishop>(Colour::White, 6, 1);

    cells[4][8].fig = std::make_unique<Queen>(Colour::Black, 4, 8);
    cells[4][1].fig = std::make_unique<Queen>(Colour::White, 4, 1);

    cells[5][8].fig = std::make_unique<King>(Colour::Black, 5, 8);

    cells[5][1].fig = std::make_unique<King>(Colour::White, 5, 1);
}

std::unique_ptr<Figure> Desk::tryToMakeMove(int old_x, int old_y, int new_x, int new_y, Colour player_colour) {
    if (this->cells[old_x][old_y].fig==nullptr)
        throw std::logic_error("There is no figure in cell!");

    if (this->cells[old_x][old_y].fig->getColour()!=player_colour)
        throw std::logic_error("It's is not your figure!");

    if (this->cells[old_x][old_y].fig->isMoveValid(new_x, new_y,
                                                   std::bind(&Desk::isEmptyCell, this, std::placeholders::_1, std::placeholders::_2),
                                                   std::bind(&Desk::isEnemyInCell, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3))){
        auto tmp_cell = std::move(this->cells[new_x][new_y].fig);
        this->cells[new_x][new_y].fig = std::move(this->cells[old_x][old_y].fig);
        this->cells[new_x][new_y].fig->setNewCell(new_x, new_y);
        this->cells[new_x][new_y].fig->getMoveCnt()++;
        //let's check if there is not check for player that make a move (in ths case move can't be valid)
        if (isCheck(player_colour)){
            //abort action
            this->cells[old_x][old_y].fig = std::move(this->cells[new_x][new_y].fig);
            this->cells[old_x][old_y].fig->setNewCell(old_x, old_y);
            this->cells[old_x][old_y].fig->getMoveCnt()--;
            this->cells[new_x][new_y].fig = std::move(tmp_cell);
            throw std::logic_error("You can't make this move because of check");
        }
        return std::move(tmp_cell);
    }
    else {
        throw std::logic_error("Your move is invalid!");
    }
}


GameExodus Desk::makeMove(int old_x, int old_y, int new_x, int new_y, Colour player_colour) {
    tryToMakeMove(old_x, old_y, new_x, new_y, player_colour);

    if(isWin(player_colour)) return GameExodus::Win;
    //if(isStalemate(player_colour))return GameExodus::Stalemate;
    if(isPawnChange(player_colour))return GameExodus::Change;

    return GameExodus::None;
}

bool Desk::isEmptyCell(int x, int y) {
    assert(x>=1 && x<=9 && y>=1 && y<=9);
    return cells[x][y].fig==nullptr;
}

bool Desk::isEnemyInCell(int x, int y, Colour self_colour) {
    assert(x>=1 && x<=9 && y>=1 && y<=9);
    assert(cells[x][y].fig!=nullptr);
    return cells[x][y].fig->getColour()==getEnemyColour(self_colour);
}

bool Desk::canFieldBeFightByEnemy(int field_x, int field_y, Colour enemy_colour) {
    for (int x=1;x<=8;x++){
        for (int y=1;y<=8;y++){
            if (cells[x][y].fig!=nullptr && cells[x][y].fig->getColour()==enemy_colour && !(field_x==x && field_y==y) &&
                cells[x][y].fig->isMoveValid(field_x, field_y, std::bind(&Desk::isEmptyCell, this, std::placeholders::_1, std::placeholders::_2),
                                             std::bind(&Desk::isEnemyInCell, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)))
                return true;
        }
    }
    return false;
}

void Desk::makeCastling(CastlingType cast_type, Colour player) {
    //https://ru.wikipedia.org/wiki/%D0%A0%D0%BE%D0%BA%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0
    //castling conditions taken from wikipedia
    if (player==Colour::White){
        if (cells[5][1].fig == nullptr || cells[5][1].fig->getType()!=Type::King)
            throw std::logic_error("There is no King in cell E1!");
        if (cells[5][1].fig->getMoveCnt()!=0)
            throw std::logic_error("King have one or more step!");

        if (cast_type==CastlingType::Short){
            if (cells[8][1].fig == nullptr || cells[8][1].fig->getType()!=Type::Rook)
                throw std::logic_error("There is no Rook in cell H1!");
            if (cells[8][1].fig->getMoveCnt()!=0)
                throw std::logic_error("Rook have one or more step!");
            if (cells[6][1].fig!=nullptr || cells[7][1].fig!= nullptr)//пока между королём и ладьей, предназначенными для рокировки, находится другая фигура (своя или чужая)
                throw std::logic_error("There is another figure between King and Rook!");
            if (canFieldBeFightByEnemy(5,1,getEnemyColour(player)) ||
                canFieldBeFightByEnemy(6,1,getEnemyColour(player)) ||
                canFieldBeFightByEnemy(7,1,getEnemyColour(player))) //если поле, пройденное королём во время рокировки, находится под боем противника; || если король перед началом рокировки находится под шахом, или после её осуществления попадает под шах.
                throw std::logic_error("Some field can be fight by enemy or check!");

            cells[7][1].fig = std::move(cells[5][1].fig);
            cells[7][1].fig->setNewCell(7,1);
            cells[6][1].fig = std::move(cells[8][1].fig);
            cells[6][1].fig->setNewCell(6, 1);
        }
        else{
            if (cells[1][1].fig == nullptr || cells[1][1].fig->getType()!=Type::Rook)
                throw std::logic_error("There is no Rook in cell H1!");
            if (cells[1][1].fig->getMoveCnt()!=0)
                throw std::logic_error("Rook have one or more step!");
            if (cells[2][1].fig!=nullptr || cells[3][1].fig!= nullptr || cells[4][1].fig!= nullptr)//пока между королём и ладьей, предназначенными для рокировки, находится другая фигура (своя или чужая)
                throw std::logic_error("There is another figure between King and Rook!");
            if (canFieldBeFightByEnemy(5,1,getEnemyColour(player)) ||
                canFieldBeFightByEnemy(4,1,getEnemyColour(player)) ||
                canFieldBeFightByEnemy(3,1,getEnemyColour(player))) //если поле, пройденное королём во время рокировки, находится под боем противника; || если король перед началом рокировки находится под шахом, или после её осуществления попадает под шах.
                throw std::logic_error("Some field can be fight by enemy or check!");

            cells[3][1].fig = std::move(cells[5][1].fig);
            cells[3][1].fig->setNewCell(3,1);
            cells[4][1].fig = std::move(cells[1][1].fig);
            cells[4][1].fig->setNewCell(4, 1);
        }
    }
    else{
        if (cells[5][8].fig == nullptr || cells[5][8].fig->getType()!=Type::King)
            throw std::logic_error("There is no King in cell E1!");
        if (cells[5][8].fig->getMoveCnt()!=0)
            throw std::logic_error("King have one or more step!");

        if (cast_type==CastlingType::Short){
            if (cells[8][8].fig == nullptr || cells[8][8].fig->getType()!=Type::Rook)
                throw std::logic_error("There is no Rook in cell H1!");
            if (cells[8][8].fig->getMoveCnt()!=0)
                throw std::logic_error("Rook have one or more step!");
            if (cells[6][8].fig!=nullptr || cells[7][8].fig!= nullptr)//пока между королём и ладьей, предназначенными для рокировки, находится другая фигура (своя или чужая)
                throw std::logic_error("There is another figure between King and Rook!");
            if (canFieldBeFightByEnemy(5,8,getEnemyColour(player)) ||
                canFieldBeFightByEnemy(6,8,getEnemyColour(player)) ||
                canFieldBeFightByEnemy(7,8,getEnemyColour(player))) //если поле, пройденное королём во время рокировки, находится под боем противника; || если король перед началом рокировки находится под шахом, или после её осуществления попадает под шах.
                throw std::logic_error("Some field can be fight by enemy or check!");

            cells[7][8].fig = std::move(cells[5][8].fig);
            cells[7][8].fig->setNewCell(7,8);
            cells[6][8].fig = std::move(cells[8][8].fig);
            cells[6][8].fig->setNewCell(6, 8);
        }
        else{
            if (cells[8][8].fig == nullptr || cells[8][8].fig->getType()!=Type::Rook)
                throw std::logic_error("There is no Rook in cell H1!");
            if (cells[1][8].fig->getMoveCnt()!=0)
                throw std::logic_error("Rook have one or more step!");
            if (cells[2][8].fig!=nullptr || cells[3][8].fig!= nullptr || cells[4][8].fig!= nullptr)//пока между королём и ладьей, предназначенными для рокировки, находится другая фигура (своя или чужая)
                throw std::logic_error("There is another figure between King and Rook!");
            if (canFieldBeFightByEnemy(5,8,getEnemyColour(player)) ||
                canFieldBeFightByEnemy(4,8,getEnemyColour(player)) ||
                canFieldBeFightByEnemy(3,8,getEnemyColour(player))) //если поле, пройденное королём во время рокировки, находится под боем противника; || если король перед началом рокировки находится под шахом, или после её осуществления попадает под шах.
                throw std::logic_error("Some field can be fight by enemy or check!");

            cells[3][8].fig = std::move(cells[5][8].fig);
            cells[3][8].fig->setNewCell(3,8);
            cells[4][8].fig = std::move(cells[1][8].fig);
            cells[4][8].fig->setNewCell(4, 8);
        }
    }

}

bool Desk::isCheck(Colour king_colour) {
    int king_x=1;
    int king_y=1;
    bool tmp_flag = false;
    for (;king_x<=8;king_x++){
        for (king_y=1;king_y<=8;king_y++){
            if (cells[king_x][king_y].fig!=nullptr && cells[king_x][king_y].fig->getType()==Type::King
                && cells[king_x][king_y].fig->getColour()==king_colour) {
                tmp_flag = true;
                break;
            }
        }
        if (tmp_flag) break;
    }
    //now we have a king pos

    //if smth will be wrong)
    assert (king_x!=9 && king_y!=9);
    assert(!(king_x==8 && king_y==8 && cells[8][8].fig!=nullptr && cells[8][8].fig->getType()==Type::King && cells[8][8].fig->getColour()==king_colour));

    //now let's check if pos of king can be attack by enemy
    return canFieldBeFightByEnemy(king_x, king_y, getEnemyColour(king_colour));
}

bool Desk::isWin(Colour win_colour) {
    //if player win enemy's king in under attack
    auto enemy = getEnemyColour(win_colour);
    if (!isCheck(enemy)) return false;

    //if enemy have at least one step to save his king it is not win
    bool res = false;
    for (int x=1;x<=8;x++){
        for (int y=1;y<=8;y++){
            res=isAnyStepToSaveKing(x, y, enemy);
            if (res) break;
        }
        if (res) break;
    }
    return !res;
}

bool Desk::isAnyStepToSaveKing(int fig_x, int fig_y, Colour colour) {
    bool res=false;
    if (cells[fig_x][fig_y].fig!=nullptr && cells[fig_x][fig_y].fig->getColour()==colour){
        for (int x=1;x<=8;x++){
            for (int y=1;y<=8;y++){
                try{
                    auto probably_deleted_figure = tryToMakeMove(fig_x, fig_y, x, y, colour);
                    //if we don't get an error move is valid
                    //let's make abort
                    this->cells[fig_x][fig_y].fig = std::move(this->cells[x][y].fig);
                    this->cells[x][y].fig = std::move(probably_deleted_figure);
                    this->cells[fig_x][fig_y].fig->setNewCell(fig_x, fig_y);
                    res = true;
                    break;
                }
                catch (...) {
                }
            }
            if (res) break;
        }
    }
    return res;
}

bool Desk::isStalemate(Colour win_colour) {
    //if player stalemate enemy's king isn't under attack
    auto enemy = getEnemyColour(win_colour);
    if (isCheck(enemy)) return false;

    //if enemy have at least one step to don't get a check king it is not stalemate
    bool res = false;
    for (int x=1;x<=8;x++){
        for (int y=1;y<=8;y++){
            res=isAnyStep(x, y, enemy);
            if (res) break;
        }
        if (res) break;
    }
    return !res;
}

bool Desk::isPawnChange(Colour player) {
    if (player==Colour::White){
        for (int x=1;x<=8;x++){
            if (cells[x][8].fig!= nullptr && cells[x][8].fig->getType()==Type::Pawn && cells[x][8].fig->getColour()==Colour::White){
                return true;
            }
        }
    }
    else{
        for (int x=1;x<=8;x++){
            if (cells[x][1].fig!= nullptr && cells[x][1].fig->getType()==Type::Pawn && cells[x][1].fig->getColour()==Colour::Black){
                return true;
            }
        }
    }
    return false;
}

void Desk::makePawnChange(Colour player, Type type) {
    if (player==Colour::White){
        for (int x=1;x<=8;x++){
            if (cells[x][8].fig!= nullptr && cells[x][8].fig->getType()==Type::Pawn && cells[x][8].fig->getColour()==Colour::White){
                switch (type) {
                    case Type::Queen:
                        cells[x][8].fig = std::make_unique<Queen>(Colour::White, x, 8, 1);
                        break;
                    case Type::Bishop:
                        cells[x][8].fig = std::make_unique<Bishop>(Colour::White, x, 8, 1);
                        break;
                    case Type::Rook:
                        cells[x][8].fig = std::make_unique<Rook>(Colour::White, x, 8, 1);
                        break;
                    case Type::Knight:
                        cells[x][8].fig = std::make_unique<Knight>(Colour::White, x, 8, 1);
                        break;
                    default:
                        throw std::logic_error("pawn can't be replace by this figure!");
                }
            }
        }
    }
    else{
        for (int x=1;x<=8;x++){
            if (cells[x][1].fig!= nullptr && cells[x][1].fig->getType()==Type::Pawn && cells[x][1].fig->getColour()==Colour::Black){
                switch (type) {
                    case Type::Queen:
                        cells[x][8].fig = std::make_unique<Queen>(Colour::Black, x, 8, 1);
                        break;
                    case Type::Bishop:
                        cells[x][8].fig = std::make_unique<Bishop>(Colour::Black, x, 8, 1);
                        break;
                    case Type::Rook:
                        cells[x][8].fig = std::make_unique<Rook>(Colour::Black, x, 8, 1);
                        break;
                    case Type::Knight:
                        cells[x][8].fig = std::make_unique<Knight>(Colour::Black, x, 8, 1);
                        break;
                    default:
                        throw std::logic_error("pawn can't be replace by this figure!");
                }
            }
        }
    }
}

bool Desk::isAnyStep(int fig_x, int fig_y, Colour colour) {
    bool res=false;
    if (cells[fig_x][fig_y].fig!=nullptr && cells[fig_x][fig_y].fig->getColour()==colour){
        for (int x=1;x<=8;x++){
            for (int y=1;y<=8;y++){
                try{
                    auto probably_deleted_figure = tryToMakeMove(fig_x, fig_y, x, y, colour);
                    res = !isCheck(colour);
                    //if we don't get an error move is valid
                    //let's make abort
                    this->cells[fig_x][fig_y].fig = std::move(this->cells[x][y].fig);
                    this->cells[x][y].fig = std::move(probably_deleted_figure);
                    this->cells[fig_x][fig_y].fig->setNewCell(fig_x, fig_y);
                    if(res)
                        break;
                }
                catch (...) {
                }
            }
            if(res) break;
        }
    }
    return res;
}

}