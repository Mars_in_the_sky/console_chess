//
// Created by serhii on 26.04.20.
//

#include <cassert>
#include <cmath>
#include "figures.hpp"

namespace chess{

Colour chess::Figure::getColour() const noexcept{
    return colour;
}

Figure::Figure(Colour c, Type t, int x, int y, int move_cnt):
colour(c), pos_x(x), pos_y(y), move_cnt(move_cnt), type(t) {}

Type Figure::getType() const noexcept{
    return type;
}

void Figure::setNewCell(int x, int y) noexcept{
    assert(x>=1 && x<=9 && y>=1 && y<=9);
    pos_x = x;
    pos_y = y;
}

bool Figure::isAllyOnNewCell(int new_x, int new_y, const is_empty_cell& isEmptyCell, const is_enemy_in_cell& isEnemyInCell) const noexcept {
    return !isEmptyCell(new_x, new_y) && !isEnemyInCell(new_x, new_y, colour);
}

int& Figure::getMoveCnt() noexcept {
    return move_cnt;
}

int Figure::getMoveCnt() const noexcept {
    return move_cnt;
}


Pawn::Pawn(Colour c, int x, int y, int move_cnt) : Figure(c, Type::Pawn, x, y, move_cnt) {}

bool Pawn::isMoveValid(int new_x, int new_y, const is_empty_cell& isEmptyCell, const is_enemy_in_cell& isEnemyInCell) {
    if (isAllyOnNewCell(new_x, new_y, isEmptyCell, isEnemyInCell)) return false;
    if (colour==Colour::White) {
        bool move = (new_x == pos_x && (new_y - pos_y) == 1 && isEmptyCell(new_x, new_y)) //если клетка на которую становимся пустая
                    || (pos_y == 2 && new_x == pos_x && (new_y - pos_y) == 2 && isEmptyCell(new_x, new_y) &&
                        isEmptyCell(new_x, new_y - 1));//первый ход пешки может быть на 2 клетки
        bool fight = !isEmptyCell(new_x, new_y) &&
                     ((new_y - pos_y) == 1 && ((new_x - pos_x) == 1 || (new_x - pos_x) == -1)); //когда мы бьем пешкой
        return bool(move || fight);
    }
    else {//Black
        bool move = (new_x == pos_x && (new_y - pos_y) == -1 && isEmptyCell(new_x, new_y)) //если клетка на которую становимся пустая
                    || (pos_y == 7 && new_x == pos_x && (new_y - pos_y) == -2 && isEmptyCell(new_x, new_y) &&
                        isEmptyCell(new_x, new_y + 1));//первый ход пешки может быть на 2 клетки
        bool fight = !isEmptyCell(new_x, new_y) &&
                     ((new_y - pos_y) == -1 && ((new_x - pos_x) == 1 || (new_x - pos_x) == -1)); //когда мы бьем пешкой
        return bool(move || fight);
    }
}


Rook::Rook(Colour c, int x, int y, int move_cnt):Figure(c, Type::Rook, x, y) {}


inline bool isRookMoveValid(int new_x, int new_y, int old_x, int old_y, const is_empty_cell& isEmptyCell){
    if (new_y==old_y) {//make a move/fight by x;
        if (new_x>old_x){//make a move to right
            bool b = true;
            for (int cur_pos = old_x+1; cur_pos<new_x; cur_pos++){//check if all cells are empty
                b&=isEmptyCell(cur_pos, new_y);
            }
            return b;
        }
        else if (new_x<old_x){
            bool b = true;
            for (int cur_pos = old_x-1; cur_pos>new_x; cur_pos--){//check if all cells are empty
                b&=isEmptyCell(cur_pos, new_y);
            }
            return b;
        }
    }//todo rewrite with std::min and std::max like a bishop
    else if (new_x==old_x){//make a move/fight by y
        if (new_y>old_y){//make a move to up
            bool b = true;
            for (int cur_pos = old_y+1; cur_pos<new_y; cur_pos++){//check if all cells are empty
                b&=isEmptyCell(new_x, cur_pos);
            }
            return b;
        }
        else if (new_y<old_y){
            bool b = true;
            for (int cur_pos = old_y-1; cur_pos>new_y; cur_pos--){//check if all cells are empty
                b&=isEmptyCell(new_x, cur_pos);
            }
            return b;
        }
    }
    return false;
}

bool Rook::isMoveValid(int new_x, int new_y, const is_empty_cell& isEmptyCell, const is_enemy_in_cell& isEnemyInCell) {
    if (isAllyOnNewCell(new_x, new_y, isEmptyCell, isEnemyInCell)) return false;
    return isRookMoveValid(new_x, new_y, pos_x, pos_y, isEmptyCell);
}

Knight::Knight(Colour c, int x, int y, int move_cnt):Figure(c, Type::Knight, x, y) {}

bool Knight::isMoveValid(int new_x, int new_y, const is_empty_cell& isEmptyCell, const is_enemy_in_cell& isEnemyInCell){
    if (isAllyOnNewCell(new_x, new_y, isEmptyCell, isEnemyInCell)) return false;
    return (std::abs(new_x-pos_x)==2 && std::abs(new_y-pos_y)==1) ||
            (std::abs(new_x-pos_x)==1 && std::abs(new_y-pos_y)==2);
}


Bishop::Bishop(Colour c, int x, int y, int move_cnt):Figure(c, Type::Bishop, x, y) {}

inline bool isBishopMoveValid(int new_x, int new_y, int old_x, int old_y, const is_empty_cell& isEmptyCell){
    bool move = (std::abs(new_x-old_x)==std::abs(new_y-old_y));
    if (move) {
        bool b=true;
        if (new_x>old_x){
            if (new_y>old_y){
                for (int cur_x = old_x + 1, cur_y = old_y + 1; cur_x<new_x && cur_y<new_y; cur_x++, cur_y++){
                    b&=isEmptyCell(cur_x, cur_y);
                }
            }
            else{
                for (int cur_x = old_x + 1, cur_y = old_y - 1; cur_x<new_x && cur_y>new_y; cur_x++, cur_y--){
                    b&=isEmptyCell(cur_x, cur_y);
                }
            }
        }
        else {
            if (new_y>old_y){
                for (int cur_x = new_x + 1, cur_y = new_y - 1; cur_x<old_x && cur_y>old_y; cur_x++, cur_y--){
                    b&=isEmptyCell(cur_x, cur_y);
                }
            }
            else {
                for (int cur_x = new_x + 1, cur_y = new_y + 1; cur_x<old_x && cur_y<old_y; cur_x++, cur_y++){
                    b&=isEmptyCell(cur_x, cur_y);
                }
            }

        }

        return b;
    }
    return false;
}
bool Bishop::isMoveValid(int new_x, int new_y, const is_empty_cell& isEmptyCell, const is_enemy_in_cell& isEnemyInCell) {
    if (isAllyOnNewCell(new_x, new_y, isEmptyCell, isEnemyInCell)) return false;
    return isBishopMoveValid(new_x, new_y, pos_x, pos_y, isEmptyCell);
}


Queen::Queen(Colour c, int x, int y, int move_cnt):Figure(c, Type::Queen, x, y)  {}

bool Queen::isMoveValid(int new_x, int new_y, const is_empty_cell& isEmptyCell, const is_enemy_in_cell& isEnemyInCell) {
    //queen like a Rook and Bishop, two in one
    if (isAllyOnNewCell(new_x, new_y, isEmptyCell, isEnemyInCell)) return false;
    return isRookMoveValid(new_x, new_y, pos_x, pos_y, isEmptyCell) || isBishopMoveValid(new_x, new_y, pos_x, pos_y, isEmptyCell);
}


King::King(Colour c, int x, int y, int move_cnt):Figure(c, Type::King, x, y) {}

bool King::isMoveValid(int new_x, int new_y, const is_empty_cell &isEmptyCell, const is_enemy_in_cell &isEnemyInCell) {
    if (isAllyOnNewCell(new_x, new_y, isEmptyCell, isEnemyInCell)) return false;
    return (std::abs(new_x-pos_x)<=1 && std::abs(new_y-pos_y)<=1 && (std::abs(new_x-pos_x)+std::abs(new_y-pos_y))>0);
}



}//namespace chess