//
// Created by serhii on 25.04.20.
//

#include <cassert>
#include <iostream>
#include <thread>
#include "user_command.hpp"

namespace chess{


inline void del_white_symbols(std::string &input_str){
    auto new_end = std::remove_if(input_str.begin(), input_str.end(), [](const auto c){
        return std::isspace(c);
    });
    input_str.erase(new_end, input_str.end());
}

inline std::optional<std::pair<int, int>> parse_one_cell(std::string& cell_pos){
    assert(cell_pos.size()==2);
    int x = std::toupper(cell_pos[0]) - 'A' +1;
    int y = cell_pos[1] - '0';
    return (x>=1 && x<=8 && y>=1 && y<=8)?std::make_optional(std::make_pair(x, y)):std::nullopt;
}

inline bool check_separator(std::string& separator){
    return separator=="->" || separator=="to" || separator=="TO" || separator=="To";
}

std::optional<Move> UserCommand::parse_move(std::string &input_str) {
    std::optional<Move> res = std::nullopt;
    //move command types:  [a1 -> b4] [a1->B4] [A1 to b2]
    //I would prefer to use here boost::string or even boost::spirit

    //delete all white symbols
    del_white_symbols(input_str);

    if (input_str.size()==6){
        std::string old_pos = input_str.substr(0,2);
        std::string separator = input_str.substr(2,2);
        std::string new_pos = input_str.substr(4, 2);

        auto old_p=parse_one_cell(old_pos);
        auto new_p=parse_one_cell(new_pos);
        if (check_separator(separator) && old_p.has_value() && new_p.has_value()){
            res = {old_p->first, old_p->second, new_p->first, new_p->second};
        }
    }
    return res;
}

const std::unordered_map<std::string, CommandType> UserCommand::commands = {
        {"move", CommandType::Move},
        {"castling", CommandType::Castling},
        {"load", CommandType::Load},
        {"save", CommandType::Save},
        {"help", CommandType::Help},
        {"start", CommandType::NewGame}
};

bool UserCommand::makeCommand(std::string &input_str) {
    auto [action_str, command_type] = define_command_type(input_str);
    bool res = false;//flase res means that we shouldn't make system("clear");
    switch (command_type) {
        case CommandType::Move:
            res = make_move(action_str);
            break;
        case CommandType::Castling:
            res = make_castling(action_str);
            break;
        case CommandType::Save:
            make_save();
            break;
        case CommandType::Load:
            res = make_load(action_str);
            break;
        case CommandType::NewGame:
            init_new_game();
            res = true;
            break;
        case CommandType::Help:
            help();
            break;
        case CommandType::Undefined:
            undefined();
            break;
    }
    return res;
}

std::pair<std::string, CommandType> UserCommand::define_command_type(std::string &input_str) {
    //first let's detect which pos don't contain white symbols
    auto start = input_str.find_first_not_of(" \t\n\f\r\v");
    auto res = std::make_pair(std::string{}, CommandType::Undefined);
    if (start!=std::string::npos) {
        for (const auto& [tag, type]:commands){
            if (input_str.compare(start, tag.size(),  tag)==0) {
                res = std::make_pair(input_str.substr(start+tag.size()), type);
                break;
            }
        }
    }
    return res;
}

bool UserCommand::make_move(std::string &input_str) {
    auto move_params = parse_move(input_str);
    if (move_params.has_value()){
        try {
            auto exodus = desk.makeMove(move_params->old_x, move_params->old_y, move_params->new_x, move_params->new_y, current_player_colour);
            //we do this here because we already knows that move is correct
            std::string action = {'m', 'o', 'v', 'e', ' ', char ('A'+move_params->old_x-1), char('0'+move_params->old_y), '-', '>', char ('A'+move_params->new_x-1), char('0'+move_params->new_y) };
            storage.addActionToCurrGame(action);
            if (exodus==GameExodus::Win){
                std::cout<<"Congratulation! "<<((current_player_colour == Colour::White) ? "White" : "Black")<<" won!"<<std::endl;
                game_running = false;
                return false;
            }
            else if (exodus==GameExodus::Stalemate){
                std::cout<<"Stalemate!"<<std::endl;
                game_running = false;
                return false;
            }
            else if (exodus==GameExodus::Change){
               makeChange();
            }
            return true;
        }
        catch (std::logic_error& ex) {
            std::cerr<<ex.what()<<std::endl;
            return false;
        }
    }
    else {
        std::cerr<<"Invalid params!"<<std::endl;
        return false;
    }
}

void UserCommand::visualization() {
    std::cout<<"Game id: "<<storage.getGameId()<<std::endl;
    desk.visualization();
}

void UserCommand::help() {
    std::cout<<"To make move print: 'move 'from' -> 'to'' or 'move 'from' to 'to''\n"
        <<"\t for example: 'f2 to f4' or 'H5 -> h8' or 'a1->B2'\n"
        <<"To make castling print: 'castling 'type of castling'\n"
        <<"\t for example: 'castling short' or 'castling long'\n"
        <<"To save game just print: 'save'\n";
    std::flush(std::cout);
}

bool UserCommand::make_castling(std::string &input_str) {
    auto castling_type = parse_castling(input_str);
    if (castling_type.has_value()){
        try {
            desk.makeCastling(castling_type.value(), current_player_colour);
            std::string action = std::string("castling ")+((castling_type.value()==CastlingType::Short)?"short":"long");
            storage.addActionToCurrGame(action);
            return true;
        }
        catch (std::logic_error& ex) {
            std::cerr<<ex.what()<<std::endl;
            return false;
        }
    }
    else {
        std::cerr<<"Invalid params!\n";
        return false;
    }


}

std::optional<CastlingType> UserCommand::parse_castling(std::string &input_str) {
    std::optional<CastlingType> res = std::nullopt;
    del_white_symbols(input_str);
    if (input_str == "long"){
        res = CastlingType::Long;
    }
    else if (input_str == "short"){
        res = CastlingType::Short;
    }
    return res;
}

void UserCommand::undefined() {
    std::cout<<"Undefined action! Type \"help\" to get help"<<std::endl;
}

void UserCommand::make_save() {
    auto id = storage.finishGame();
    game_running = false;
    current_player_colour = Colour::White;
    std::cout<<"Your game has been saved. To load this game type \"load "<<id<<"\""<<std::endl;
}

bool UserCommand::make_load(std::string &input_str) {
    del_white_symbols(input_str);
    try {
        auto id = std::stol(input_str);
        if (game_running) {
            auto old_id = storage.finishGame();
            std::cout << "Your old game has been saved. To load this game type \"load " << old_id << "\"" << std::endl;
        }

        auto game = storage.getGame(id);

        current_player_colour=Colour::White;
        desk = Desk();
        for(auto& step:game){
            desk.visualization();
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            makeCommand(step);
            current_player_colour = getEnemyColour(current_player_colour);
            system("clear");
        }

        storage.startGame(id);
        game_running = true;
        return true;
    }
    catch (std::exception& ex) {
        std::cerr<<"Error: "<<ex.what()<<std::endl;
        game_running = false;
        return false;
    }
}

UserCommand::UserCommand(std::string working_directory):storage(working_directory) {
}

[[noreturn]] void UserCommand::run() {
    bool success = true;
    game_running = false;
    while (true){
        if(game_running) {
            if (success) visualization();
            std::cout << "Player " << ((current_player_colour == Colour::White) ? "White" : "Black") << "~$:";
            std::string input_str;
            std::getline(std::cin, input_str);
            if (std::cin.fail()) {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::cout << "Wrong input!";
            } else {
                success = makeCommand(input_str);
            }
            if (success) {
                system("clear");//FOR UNIX LIKE SYSTEMS
                current_player_colour = getEnemyColour(current_player_colour);
            }
        }
        else {
            std::cout << "\nTo start new game type \"start\"." << std::endl;
            std::cout << "To load game type \"load 'game_id'\"." << std::endl;
            std::cout << "To get help type \"help\"." << std::endl;
            std::cout << "chess~$:";
            std::string input_str;
            std::getline(std::cin, input_str);
            if (std::cin.fail()) {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::cout << "Wrong input!";
            } else {
                game_running = makeStartingCommand(input_str);
                success = game_running;
            }
        }
    }
}

void UserCommand::init_new_game() {
    storage.finishGame();
    auto game_id = storage.startGame();
    desk = Desk();
    system("clear");
    std::cout<<"Starting new game. Game id: "<<game_id<<std::endl;
    game_running = true;
}

bool UserCommand::makeStartingCommand(std::string &input_str) {
    auto [action_str, command_type] = define_command_type(input_str);
    bool res = true;//false res means that something wrong in input str or if we should't start new game
    switch (command_type) {
        case CommandType::Load:
            res = make_load(action_str);
            break;
        case CommandType::NewGame:
            init_new_game();
            break;
        case CommandType::Help:
            help();
            res = false;
            break;
        default:
            undefined();
            res=false;
            break;
    }
    return res;
}

void UserCommand::makeChange() {
    bool success = false;
    while (!success) {
        try {
            std::cout << "Change figure for change~$:" << std::endl;
            std::string input_str;
            std::getline(std::cin, input_str);
            if (std::cin.fail()) {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::cout << "Wrong input!";
            } else {
                success = true;
                if (input_str == "Rook" || input_str == "rook") {
                    desk.makePawnChange(current_player_colour, Type::Rook);
                } else if (input_str == "Queen" || input_str == "queen") {
                    desk.makePawnChange(current_player_colour, Type::Queen);
                } else if (input_str == "bishop" || input_str == "Bishop") {
                    desk.makePawnChange(current_player_colour, Type::Bishop);
                } else if (input_str == "Knight" || input_str == "knight") {
                    desk.makePawnChange(current_player_colour, Type::Knight);
                } else {
                    success = false;
                }
            }
        }
        catch (std::exception& ex) {
            std::cout<<ex.what()<<std::endl;
        }
    }
}


}

