//
// Created by serhii on 28.04.20.
//
#include "games_storage.hpp"

namespace chess{

long GamesStorage::startGame(long id) {
    finishGame();
    if (id!=0){
        game_id = id;
        output.open(resources.string()+"game_"+std::to_string(game_id)+".chs", std::ios_base::app);
    }
    else {
        game_id = time(nullptr)%1000;
        output.open(resources.string() + "game_" + std::to_string(game_id) + ".chs");
    }
    return game_id;
}

long GamesStorage::finishGame() {
    output.close();
    return game_id;
}

void GamesStorage::addActionToCurrGame(std::string &act) {
    output<<act<<std::endl;
}

std::vector<std::string> GamesStorage::getGame(long id) {
    std::string target_filename = "game_"+std::to_string(id)+".chs";

    bool res = false;
    for (auto& p:std::filesystem::directory_iterator(resources)){
        res|=(target_filename==p.path().filename());
    }
    if (!res){
        throw std::logic_error("There is not file in directory!");
    }
    std::vector<std::string> game;
    std::string tmp;
    std::ifstream input(resources.string()+target_filename);
    while (!input.fail()){
        std::getline(input, tmp);
        if (!tmp.empty()) game.push_back(tmp);
    }
    return game;
}

GamesStorage::GamesStorage(std::string &path):resources(path) {}

long GamesStorage::getGameId() const noexcept {
    return game_id;
}


}

